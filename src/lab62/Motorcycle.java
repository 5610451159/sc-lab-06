package lab62;

public class Motorcycle extends Vehicle {
	
	public Motorcycle(String brand) {
		super(brand) ;	
	}
	
	@Override
	public String wheel() {
		return "2" ;
	}

}
