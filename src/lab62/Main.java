package lab62;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car car = new Car("Brand : ISUZU") ;
		Motorcycle motorcycle = new Motorcycle("Brand : Ducati") ;
		
		System.out.println(car) ;
		System.out.println(motorcycle) ;
		
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>() ;
		vehicles.add(car) ;
		vehicles.add(motorcycle) ;
		
		for (Vehicle vehicle : vehicles) {
			System.out.println(vehicle.wheel()) ;
		}

		Wheel wheel = new Wheel() ;
		System.out.println(wheel.NumberOfWheel(car)) ;
		System.out.println(wheel.NumberOfWheel(motorcycle)) ;
		
	}

}
