package lab62;

public class Car extends Vehicle {
	
	public Car(String brand) {
		super(brand) ;	
	}
	
	@Override
	public String wheel() {
		return "4" ;
	}
}
