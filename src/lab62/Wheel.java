package lab62;

public class Wheel {
	
	public String NumberOfWheel(Vehicle vehicle) {
		return "Vehicle have " + vehicle.wheel() + " wheels" ;
	}
	
	public String NumberOfWheel(Car car) {
		return "Car have " + car.wheel() + " wheels" ;
	}
	
	public String NumberOfWheel(Motorcycle motorcycle) {
		return "Motorcycle have " + motorcycle.wheel() + " wheels" ;
	}
}
