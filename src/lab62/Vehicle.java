package lab62;

public abstract class Vehicle {
	String brand ;
	
	public Vehicle(String brand) {
		this.brand = brand ;	
	}
	
	public String toString() {
		return brand ;
	}
	
	public abstract String wheel() ;
}
